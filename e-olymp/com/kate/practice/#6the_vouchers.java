import java.util.Scanner;

public class Main {

    public static int max;
    public static int n;
    public static int[] days;
    public static int[] prices;

    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        n = scn.nextInt();
        days=new int[n];
        prices=new int[n];
        for (int i = 0; i < n; i++) {
            days[i]=scn.nextInt();
            prices[i]=scn.nextInt();
        }
        recursive(1,0);
        System.out.println(max);
    }

    public static void recursive(int day,int total){
        int temp=0;
        for(int i=0;i<n;++i)
            if (days[i] >= day) {
                int amount = (days[i] - day + 1) * prices[i];
                temp = days[i];
                days[i]= 0;
                recursive(day + 1, total + amount);
                days[i]=temp;
                temp=1;
            }
        if(temp==0)
            max=Math.max(max,total);

    }
}
