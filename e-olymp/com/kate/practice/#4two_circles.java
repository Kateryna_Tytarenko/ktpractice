import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scn=new Scanner(System.in);
        double x1=scn.nextDouble();
        double y1=scn.nextDouble();
        double r1=scn.nextDouble();
        double x2=scn.nextDouble();
        double y2=scn.nextDouble();
        double r2=scn.nextDouble();
        System.out.println(calcResult(x1,y1,r1,x2,y2,r2));

    }

    public static int calcResult(double x1,double y1,double r1,double x2,double y2,double r2){
        double dist=Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
        if(dist==0&&r1==r2)
            return -1;
        if(dist==r1+r2||dist+r1==r2||dist+r2==r1)
            return 1;
        if(dist>r1+r2||dist+r1<r2||dist+r2<r1)
            return 0;
        return 2;
    }
}